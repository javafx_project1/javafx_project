package com.project1.BookStrore;

import com.project1.MainPage;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.ScrollPane.ScrollBarPolicy;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;

public class Bookstorepage1 {

    private MainPage app;
    private GridPane view;


    public Bookstorepage1(MainPage app){
        this.app=app;
        initialize();
    }

    private void initialize(){

        Button LoginButton = new Button("Login");
        LoginButton.setPrefWidth(130);
        LoginButton.setPrefHeight(40);
        //LoginButton.setFont(Font.font("", FontWeight.BOLD, 15));
        //LoginButton.setTextFill(Color.WHITE);
        LoginButton.setStyle("-fx-background-color:Blue");
        
        
        LoginButton.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                app.Login1();
            }
            
        });
        view = new GridPane();
        
        Button SignUp = new Button("Sign Up");
        SignUp.setPrefWidth(130);
        SignUp.setPrefHeight(40);
        SignUp.setStyle("-fx-background-color:green");
        
        HBox hb1 = new HBox();
        hb1.setPrefHeight(10);
        hb1.setPrefWidth(1700);
        hb1.setStyle("-fx-background-color:blue");

        HBox hb3 = new HBox();
        hb3.setPrefWidth(1100);
        hb3.setPrefHeight(80);

        HBox hb4 = new HBox(45,LoginButton,SignUp);
        hb4.setPrefWidth(600);
        hb4.setPrefHeight(80);
        hb4.setAlignment(Pos.CENTER);

        HBox hb2 = new HBox( hb3,hb4);
        hb2.setPrefHeight(80);
        hb2.setPrefWidth(1700);
        //hb2.setLayoutY(70);
        hb2.setStyle("-fx-background-color:White");

        //Image homeimg = new Image("home.jpeg");
        //ImageView imgView1 = new ImageView(homeimg);
        Button home = new Button("Home");
        home.setPrefWidth(100);
        home.setStyle("-fx-background-color:transparent");
        //home.setTextFill(Color.WHITE);
        home.setFont(Font.font("", FontWeight.BOLD, 18));

        home.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                app.Home1();
            }
            
        });

        Button BookStore = new Button("Book Store");
        BookStore.setPrefWidth(150);
        BookStore.setStyle("-fx-background-color:transparent");
        //BookStore.setTextFill(Color.WHITE);
        BookStore.setFont(Font.font("", FontWeight.BOLD, 18));
        Button Quiz = new Button("Quiz");
        Quiz.setPrefWidth(100);
        Quiz.setStyle("-fx-background-color:transparent");
        //Quiz.setTextFill(Color.WHITE);
        Quiz.setFont(Font.font("", FontWeight.BOLD, 18));
        Quiz.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                app.Quizhome1();
            }
            
        });

        HBox hb6 = new HBox(30,home, BookStore, Quiz);
        hb6.setPrefHeight(60);
        hb6.setPrefWidth(700);
        hb6.setAlignment(Pos.CENTER_LEFT);

        Image image1 = new Image("menu.png");
        ImageView imageView = new ImageView(image1);
        imageView.setFitWidth(20);
        imageView.setFitHeight(20);
        imageView.setPreserveRatio(false);
        Button setting = new Button();
        setting.setGraphic(imageView);
        //setting.setPrefWidth(120);
        //setting.setPrefHeight(40);
        

        HBox hb7 = new HBox(setting);
        hb7.setPrefHeight(60);
        hb7.setPrefWidth(960);
        hb7.setAlignment(Pos.CENTER_RIGHT);

        

        HBox hb5 = new HBox(hb6, hb7);
        hb5.setPrefWidth(1700);
        hb5.setPrefHeight(60);
        hb5.setStyle("-fx-background-color:blue");
        

       // hb5.setAlignment(Pos.CENTER);
       VBox vb1 = new VBox(hb1, hb2, hb5);
//----------------------------------------------------------------------------------------------------


        

        HBox h2 = new HBox();
        h2.setPrefHeight(300);
        h2.setMaxWidth(1400);
        h2.setStyle("-fx-border-color:red");
        HBox h1 = new HBox();
        h1.setPrefHeight(50);
        h1.setPrefWidth(130);
        //h1.setStyle("-fx-background-color:green");

        HBox h3 = new HBox();
        h3.setPrefHeight(300);
        h3.setMaxWidth(1400);
        h3.setStyle("-fx-border-color:black");

        HBox h4 = new HBox();
        h4.setPrefHeight(300);
        h4.setMaxWidth(1400);
        h4.setStyle("-fx-border-color:yellow");

        HBox h5 = new HBox();
        h5.setPrefHeight(300);
        h5.setMaxWidth(1400);
        h5.setStyle("-fx-border-color:aqua");


        VBox vBox1 = new VBox(40,h1, h2, h3, h4, h5);
        vBox1.setPrefWidth(1700);
        vBox1.setPrefHeight(1400);
        //vBox1.setStyle("-fx-background-color:blue");
        vBox1.setAlignment(Pos.CENTER);

        ScrollPane Bookscorll = new ScrollPane();
        Bookscorll.setContent(vBox1);
        Bookscorll.setVbarPolicy(ScrollBarPolicy.ALWAYS);
        Bookscorll.setHbarPolicy(ScrollBarPolicy.NEVER);
        
        
        



        HBox hBox1 = new HBox(vBox1, Bookscorll);
        hBox1.setPrefHeight(850);
        hBox1.setPrefWidth(1700);
        //hBox1.setStyle("-fx-background-color:black");
        hBox1.setAlignment(Pos.CENTER);

        VBox vb3 = new VBox(vb1, hBox1);
        vb3.setMinHeight(1700);
    

        view.add(vb3, 1, 0);
       
        //view.setStyle("-fx-background-color:yellow");



    }

    public GridPane getView(){
        return view;
    }
    
}