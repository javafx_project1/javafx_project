package com.project1;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.ScrollPane.ScrollBarPolicy;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;

public class homePage {

    private MainPage app;
    private GridPane view;


    public homePage(MainPage app){
        this.app=app;
        initialize();
    }

    private void initialize(){

        Button LoginButton = new Button("Login");
        LoginButton.setPrefWidth(130);
        LoginButton.setPrefHeight(40);
        //LoginButton.setFont(Font.font("", FontWeight.BOLD, 15));
        //LoginButton.setTextFill(Color.WHITE);
        LoginButton.setStyle("-fx-background-color:Blue");
        
        
        LoginButton.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                app.Login1();
            }
            
        });
        view = new GridPane();
        
        Button SignUp = new Button("Sign Up");
        SignUp.setPrefWidth(130);
        SignUp.setPrefHeight(40);
        SignUp.setStyle("-fx-background-color:green");
        
        HBox hb1 = new HBox();
        hb1.setPrefHeight(10);
        hb1.setPrefWidth(1700);
        hb1.setStyle("-fx-background-color:blue");

        // Button button12 = new Button("asddf");

        HBox hb3 = new HBox();
        hb3.setPrefWidth(1100);
        hb3.setPrefHeight(80);


        HBox hb4 = new HBox(45,LoginButton,SignUp);
        hb4.setPrefWidth(600);
        hb4.setPrefHeight(80);
        hb4.setAlignment(Pos.CENTER);

        HBox hb2 = new HBox(hb3,hb4);
        hb2.setPrefHeight(80);
        hb2.setPrefWidth(1700);
        //hb2.setLayoutY(70);
        hb2.setStyle("-fx-background-color:White");

        //Image homeimg = new Image("home.jpeg");
        //ImageView imgView1 = new ImageView(homeimg);
        Button home = new Button("Home");
        home.setPrefWidth(100);
        home.setStyle("-fx-background-color:transparent");
        //home.setTextFill(Color.WHITE);
        home.setFont(Font.font("", FontWeight.BOLD, 18));
        Button BookStore = new Button("Book Store");
        BookStore.setPrefWidth(150);
        BookStore.setStyle("-fx-background-color:transparent");
        //BookStore.setTextFill(Color.WHITE);
        BookStore.setFont(Font.font("", FontWeight.BOLD, 18));

        BookStore.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                app.bookstrore();
            }
            
        });


        Button Quiz = new Button("Quiz");
        Quiz.setPrefWidth(100);
        Quiz.setStyle("-fx-background-color:transparent");
        //Quiz.setTextFill(Color.WHITE);
        Quiz.setFont(Font.font("", FontWeight.BOLD, 18));

        Quiz.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                app.Quizhome1();
            }
            
        });

        HBox hb6 = new HBox(30,home, BookStore, Quiz);
        hb6.setPrefHeight(60);
        hb6.setPrefWidth(700);
        hb6.setAlignment(Pos.CENTER_LEFT);

        Image image1 = new Image("menu.png");
        ImageView imageView = new ImageView(image1);
        imageView.setFitWidth(20);
        imageView.setFitHeight(20);
        imageView.setPreserveRatio(false);
        Button setting = new Button();
        setting.setGraphic(imageView);
        //setting.setPrefWidth(120);
        //setting.setPrefHeight(40);
        

        HBox hb7 = new HBox(setting);
        hb7.setPrefHeight(60);
        hb7.setPrefWidth(960);
        hb7.setAlignment(Pos.CENTER_RIGHT);

        

        HBox hb5 = new HBox(hb6, hb7);
        hb5.setPrefWidth(1700);
        hb5.setPrefHeight(60);
        hb5.setStyle("-fx-background-color:blue");

       // hb5.setAlignment(Pos.CENTER);

        // Image image = new Image("back.avif");
        // ImageView ig = new ImageView(image);

        // Button seeallbtn = new Button("See All");
        // HBox d1 = new HBox(seeallbtn);
        
        // d1.setPrefWidth(1920);
        // d1.setPrefHeight(20);
        // d1.setAlignment(Pos.CENTER_RIGHT);
        //d1.setLayoutY(300);
        //d1.setStyle("-fx-background-color:black");

        //ScrollBar1
        
       

        VBox vb1 = new VBox(hb1, hb2, hb5);
        //vb1.setStyle("-fx-background-color:yellow");
     //D-----------------------------------------------------------------------------------------------------   



        HBox DBox = new HBox();
        DBox.setPrefHeight(330);
        DBox.setPrefWidth(1600);
        DBox.setStyle("-fx-background-color:white");
       
        //DBox.setStyle("-fx-border-radius: 20px;"+"-fx-border-color:black;");

        ScrollPane Dscorll = new ScrollPane();
        Dscorll.setContent(DBox);
        Dscorll.setHbarPolicy(ScrollBarPolicy.AS_NEEDED);
        Dscorll.setVbarPolicy(ScrollBarPolicy.NEVER);
        Dscorll.setMaxWidth(1400);
        Dscorll.setStyle("-fx-border-color:black");
        

        HBox DhBox = new HBox(DBox,Dscorll);
        DhBox.setPrefWidth(1400);
        DhBox.setPrefHeight(350);
       // DhBox.setStyle("-fx-border-color:blue");
        DhBox.setAlignment(Pos.CENTER);




        //P-----------------------------------------------------------------------------------------------------
        
        

        HBox PBox = new HBox();
        PBox.setPrefWidth(1600);
        PBox.setPrefHeight(330);
        PBox.setStyle("-fx-background-color:white");
       // PBox.setStyle("-fx-border-color:black");
        
        ScrollPane Pscroll = new ScrollPane();
        Pscroll.setHbarPolicy(ScrollBarPolicy.ALWAYS);
        Pscroll.setVbarPolicy(ScrollBarPolicy.NEVER);
        Pscroll.setContent(PBox);
        Pscroll.setMaxWidth(1400);
        Pscroll.setStyle("-fx-border-color:black");

        HBox PhBox = new HBox(PBox,Pscroll);
        PhBox.setPrefHeight(350);
        PhBox.setPrefWidth(1400);
        //PhBox.setStyle("-fx-border-color:green");
        PhBox.setAlignment(Pos.CENTER);

        VBox vb2 = new VBox(60,DhBox, PhBox);
        //vb2.setPrefWidth(1000);
        //vb2.setStyle("-fx-background-color:yellow");
        vb2.setAlignment(Pos.CENTER);

        VBox vb3 = new VBox(70,vb1,vb2);
        //vb3.setStyle("-fx-background-color:black");
        vb3.setAlignment(Pos.CENTER);
    

        view.add(vb3, 0, 0);
       
        //view.setStyle("-fx-background-image:url('')");



    }

    public GridPane getView(){
        return view;
    }
    
}
