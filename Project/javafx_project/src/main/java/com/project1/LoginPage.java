package com.project1;

import java.io.FileInputStream;
import java.io.IOException;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;

public class LoginPage {
    private MainPage app;
    private GridPane view;
    private FirebaseService firebaseService;

    public LoginPage(MainPage app){
        this.app = app;
        initialize();
    }

    public void initialize(){

         try{
            FileInputStream serviceAccount = new FileInputStream("javafx_project/src/main/resources/fx-my-authen.json");

            @SuppressWarnings("deprecation")
            FirebaseOptions options = new FirebaseOptions.Builder()
                .setCredentials(GoogleCredentials.fromStream(serviceAccount))
                .setDatabaseUrl("https://fx-auth-fb-a1621-default-rtdb.asia-southeast1.firebasedatabase.app/")
                .build();
            FirebaseApp.initializeApp(options);
        } catch(IOException e){
            e.printStackTrace();

        }

        view = new GridPane();

        Button LoginButton = new Button("Login");
        LoginButton.setPrefWidth(130);
        LoginButton.setPrefHeight(40);
        //LoginButton.setFont(Font.font("", FontWeight.BOLD, 15));
        //LoginButton.setTextFill(Color.WHITE);
        LoginButton.setStyle("-fx-background-color:Blue");
        
        
        LoginButton.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                app.Home1();
            }
            
        });
        view = new GridPane();
        
        Button SignUp = new Button("Sign Up");
        SignUp.setPrefWidth(130);
        SignUp.setPrefHeight(40);
        SignUp.setStyle("-fx-background-color:green");
        
        HBox hb1 = new HBox();
        hb1.setPrefHeight(10);
        hb1.setPrefWidth(1700);
        hb1.setStyle("-fx-background-color:blue");

        HBox hb3 = new HBox();
        hb3.setPrefWidth(1100);
        hb3.setPrefHeight(80);

        HBox hb4 = new HBox(45,LoginButton,SignUp);
        hb4.setPrefWidth(600);
        hb4.setPrefHeight(80);
        hb4.setAlignment(Pos.CENTER);

        HBox hb2 = new HBox(hb3, hb4);
        hb2.setPrefHeight(80);
        hb2.setPrefWidth(1700);
        //hb2.setLayoutY(70);
        hb2.setStyle("-fx-background-color:White");

        //Image homeimg = new Image("home.jpeg");
        //ImageView imgView1 = new ImageView(homeimg);
        Button home = new Button("Home");
        home.setPrefWidth(100);
        home.setStyle("-fx-background-color:blue");
        home.setTextFill(Color.WHITE);
        home.setFont(Font.font("", FontWeight.BOLD, 20));

        home.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                app.Home1();
            }
            
        });
        Button BookStore = new Button("Book Store");
        BookStore.setPrefWidth(150);
        BookStore.setStyle("-fx-background-color:blue");
        BookStore.setTextFill(Color.WHITE);
        BookStore.setFont(Font.font("", FontWeight.BOLD, 20));

        BookStore.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                app.bookstrore();
            }
            
        });
        Button Quiz = new Button("Quiz");
        Quiz.setPrefWidth(100);
        Quiz.setStyle("-fx-background-color:blue");
        Quiz.setTextFill(Color.WHITE);
        Quiz.setFont(Font.font("", FontWeight.BOLD, 20));

        Quiz.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                app.Quizhome1();
            }
            
        });

        HBox hb6 = new HBox(30,home, BookStore, Quiz);
        hb6.setPrefHeight(60);
        hb6.setPrefWidth(700);
        hb6.setAlignment(Pos.CENTER_LEFT);

        Button setting = new Button("Setting");
        setting.setPrefWidth(120);
        setting.setPrefHeight(40);
        

        HBox hb7 = new HBox(setting);
        hb7.setPrefHeight(60);
        hb7.setPrefWidth(960);
        hb7.setAlignment(Pos.CENTER_RIGHT);

        

        HBox hb5 = new HBox(hb6, hb7);
        hb5.setPrefWidth(1700);
        hb5.setPrefHeight(60);
        hb5.setStyle("-fx-background-color:blue");
        // hb5.setAlignment(Pos.CENTER);

        Label UserName = new Label("User Name:");
        TextField emailField = new TextField();
        
        Label Pass = new Label("Password:");
        PasswordField passwordField = new PasswordField();
        Label lb = new Label();

        firebaseService = new FirebaseService(this,emailField, passwordField);

        Button submit = new Button("Login");
        submit.setPrefWidth(100);
        submit.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
               firebaseService.login();
                
            }
            
        });

        Button signbutton = new Button("sigh Up");
        signbutton.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                firebaseService.signUp();
                
            }
            
        });

        HBox subhb = new HBox(20,submit, signbutton);
        subhb.setPrefWidth(30);
        subhb.setPrefHeight(50);
        subhb.setAlignment(Pos.CENTER);

        VBox vb2 = new VBox(10,UserName, emailField, Pass, passwordField, subhb, lb);
        vb2.setPrefWidth(400);
        vb2.setPrefHeight(200);
        vb2.setAlignment(Pos.CENTER_LEFT);
        vb2.setStyle("-fx-border-color:Black");
        //vb2.setStyle("-fx-background-color:Aqua");

        HBox hb9 = new HBox(vb2);
        hb9.setPrefWidth(1700);
        hb9.setPrefHeight(400);
        hb9.setAlignment(Pos.CENTER);

        //Image image = new Image("back.avif");
        //ImageView ig = new ImageView(image);
        

        VBox vb1 = new VBox(hb1, hb2, hb5);
        VBox vb3 = new VBox(hb9);

        VBox vb4 = new VBox(100,vb1, vb3);
    

        view.add(vb4, 1, 0);
       
        view.setStyle("-fx-background-color:Aqua");
        
        

    }
    public GridPane getView(){
        return view;
    }

    
    
}
