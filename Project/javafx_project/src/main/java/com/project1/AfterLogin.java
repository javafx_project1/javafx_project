package com.project1;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;

public class AfterLogin {

    private MainPage app;
    private GridPane view;


    public AfterLogin(MainPage app){
        this.app=app;
        initialize();
    }

    private void initialize(){

        Button carButton = new Button("Cart");
        carButton.setPrefWidth(130);
        carButton.setPrefHeight(40);
        //LoginButton.setFont(Font.font("", FontWeight.BOLD, 15));
        //LoginButton.setTextFill(Color.WHITE);
        carButton.setStyle("-fx-background-color:Blue");
        
        
        carButton.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                app.Cart1();
            }
            
        });
        view = new GridPane();
        
        Button SignUp = new Button("Log Out");
        SignUp.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                app.Home1();
            }
            
        });
        SignUp.setPrefWidth(130);
        SignUp.setPrefHeight(40);
        SignUp.setStyle("-fx-background-color:green");
        
        HBox hb1 = new HBox();
        hb1.setPrefHeight(10);
        hb1.setPrefWidth(1700);
        hb1.setStyle("-fx-background-color:blue");

        HBox hb3 = new HBox();
        hb3.setPrefWidth(1100);
        hb3.setPrefHeight(80);

        HBox hb4 = new HBox(45,carButton,SignUp);
        hb4.setPrefWidth(600);
        hb4.setPrefHeight(80);
        hb4.setAlignment(Pos.CENTER);

        HBox hb2 = new HBox(hb3, hb4);
        hb2.setPrefHeight(80);
        hb2.setPrefWidth(1700);
        //hb2.setLayoutY(70);
        hb2.setStyle("-fx-background-color:White");
        hb2.setStyle("-fx-border-color:black");

        //Image homeimg = new Image("home.jpeg");
        //ImageView imgView1 = new ImageView(homeimg);
        Button home = new Button("Home");
        home.setPrefWidth(100);
        home.setStyle("-fx-border-color:black");
       // home.setTextFill(Color.WHITE);
        home.setFont(Font.font("", FontWeight.BOLD, 15));


        Button BookStore = new Button("Book Store");
        BookStore.setPrefWidth(150);
        BookStore.setStyle("-fx-border-color:black");
        //BookStore.setTextFill(Color.WHITE);
        BookStore.setFont(Font.font("", FontWeight.BOLD, 15));

        BookStore.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
               app.bookstrore();
            }
            
        });

        Button Quiz = new Button("Quiz");
        Quiz.setPrefWidth(100);
        Quiz.setStyle("-fx-border-color:black");
        //Quiz.setTextFill(Color.WHITE);
        Quiz.setFont(Font.font("", FontWeight.BOLD, 15));

        Quiz.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                app.Quizhome1();
            }
            
        });

        HBox hb6 = new HBox(30,home, BookStore, Quiz);
        hb6.setPrefHeight(60);
        hb6.setPrefWidth(700);
        hb6.setAlignment(Pos.CENTER_LEFT);

        Button setting = new Button("Setting");
        setting.setPrefWidth(120);
        setting.setPrefHeight(40);
        

        HBox hb7 = new HBox(setting);
        hb7.setPrefHeight(60);
        hb7.setPrefWidth(960);
        hb7.setAlignment(Pos.CENTER_RIGHT);

        

        HBox hb5 = new HBox(hb6, hb7);
        hb5.setPrefWidth(1700);
        hb5.setPrefHeight(60);
        hb5.setStyle("-fx-background-color:blue");

       // hb5.setAlignment(Pos.CENTER);

        Image image = new Image("solid.jpg");
        ImageView ig = new ImageView(image);
        ig.setFitWidth(1700);
        ig.setFitHeight(800);
        HBox hb10 = new HBox(ig);
        hb10.setPrefWidth(1700);
        hb10.setPrefHeight(800);
        hb10.setStyle("-fx-border-color:black");
        

        VBox vb1 = new VBox(hb1, hb2, hb5, hb10);
    

        view.add(vb1, 1, 0);
       
        //view.setStyle("-fx-background-image:url('https://img.freepik.com/free-photo/digital-world-banner-background-remixed-from-public-domain-by-nasa_53876-124622.jpg?semt=sph')");



    }

    public GridPane getView(){
        return view;
    }
    
}