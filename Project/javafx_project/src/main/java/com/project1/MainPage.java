package com.project1;

import com.project1.BookStrore.Bookstorepage1;
import com.project1.BookStrore.cart;
import com.project1.Quiz.QuizhomePage;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class MainPage extends Application{
    private static Stage prStage;
    private Scene homepageScane, loginScene, bookstScenesScene, quizhomeScene, cartScene;
    private static Scene AfterlogScene;

    private homePage homePage1;
    private LoginPage loginPage;
    private AfterLogin AfterlogPage;
    private Bookstorepage1 bookStore;
    private QuizhomePage Quiz;
    private cart cartpage;

    @Override
    public void start(Stage prStage) throws Exception {
        this.prStage = prStage;
        homePage1 = new homePage(this);
        loginPage = new LoginPage(this);
        AfterlogPage = new AfterLogin(this);
        bookStore = new Bookstorepage1(this);
        Quiz = new QuizhomePage(this);
        cartpage = new cart(this);



        homepageScane = new Scene(homePage1.getView(),1700, 1000);
        loginScene = new Scene(loginPage.getView(), 1700, 1000);
        AfterlogScene = new Scene(AfterlogPage.getView(), 1700, 1000);
        bookstScenesScene = new Scene(bookStore.getView(), 1700,1000);
        quizhomeScene = new Scene(Quiz.getView(), 1700, 1000);
        cartScene = new Scene(cartpage.getView(), 1700, 1000);
        prStage.setScene(homepageScane);
        prStage.show();
        
    }
    public void Home1(){
        prStage.setScene(homepageScane);
    }
    public void Login1(){
        prStage.setScene(loginScene);
    }
    public static void Afterlog1(){
        prStage.setScene(AfterlogScene);
    }
    public void bookstrore(){
        prStage.setScene(bookstScenesScene);
    }

    public void Quizhome1(){
        prStage.setScene(quizhomeScene);
    }

    public void Cart1(){
        prStage.setScene(cartScene);
    }

    
    
}
