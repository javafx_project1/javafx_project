package com.project1.Homepage.deaseasePage.Deseases;

import com.project1.connectionpage.ConnectionPage;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.ScrollPane.ScrollBarPolicy;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

public class Cancer1 {
    private ConnectionPage app;
    private GridPane view;

    public Cancer1(ConnectionPage app) {
        this.app = app;
        initialize();
    }

    public void initialize(){

        view = new GridPane();
        Button back = new Button("back");
        back.setPrefWidth(100);
        back.setAlignment(Pos.CENTER);
        back.setStyle("-fx-background-color:White");
        back.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
               app.Deseasese();
            }
            
        });

        HBox hBox = new HBox(10,back);
        hBox.setPrefHeight(100);
        hBox.setPrefWidth(1700); 
        hBox.setAlignment(Pos.CENTER_LEFT);
        hBox.setStyle("-fx-background-color:Blue");
    

        VBox vb1box = new VBox();
        vb1box.setPrefHeight(300);
        vb1box.setPrefWidth(555);

    //add image here in place of Label

        Label l1 = new Label("Image");
        l1.setAlignment(Pos.CENTER);

        VBox vb2box = new VBox(l1);
        l1.setAlignment(Pos.CENTER);
        vb2box.setPrefHeight(300);
        vb2box.setPrefWidth(555);
        vb2box.setStyle("-fx-background-color:aqua");

        VBox vb3box = new VBox();
        vb3box.setPrefHeight(300);
        vb3box.setPrefWidth(555);


        HBox hb1Box = new HBox(20,vb1box,vb2box,vb3box);
        hb1Box.setPrefWidth(1700);
        hb1Box.setPrefHeight(350);

        VBox vbh21Box = new VBox();
        vbh21Box.setPrefHeight(2100);
        vbh21Box.setPrefWidth(100);
        vbh21Box.setAlignment(Pos.CENTER_LEFT);

        VBox vbh31Box = new VBox();
        vbh31Box.setPrefHeight(2100);
        vbh31Box.setPrefWidth(1500);
        vbh31Box.setAlignment(Pos.CENTER);
        vbh31Box.setStyle("-fx-background-color:Green");

        VBox vbh41Box = new VBox();
        vbh41Box.setPrefHeight(2100);
        vbh41Box.setPrefWidth(100);
        vbh41Box.setAlignment(Pos.CENTER_RIGHT);
    
        HBox hb2Box = new HBox(vbh21Box,vbh31Box,vbh41Box);
        vbh21Box.setAlignment(Pos.CENTER_LEFT);
        vbh31Box.setAlignment(Pos.CENTER);
        vbh41Box.setAlignment(Pos.CENTER_RIGHT);
        hb2Box.setAlignment(Pos.CENTER);
        hb2Box.setPrefHeight(2100);
        hb2Box.setPrefWidth(1500);
        //hb2Box.setStyle("-fx-background-color:Green");

        VBox vBox = new VBox(10,hb1Box,hb2Box);
        HBox hal = new HBox(vBox);

        ScrollPane scp = new ScrollPane();
        scp.setContent(hal);
        scp.setPrefHeight(2100);
        scp.setPrefWidth(1700);
        scp.setVbarPolicy(ScrollBarPolicy.ALWAYS);
        scp.setHbarPolicy(ScrollBarPolicy.NEVER);

        VBox vbs = new VBox(10,hBox,hal,scp);
        vbs.setAlignment(Pos.CENTER);
        hb2Box.setAlignment(Pos.CENTER);


        Group gp = new Group(vbs);

        view.add(gp, 0, 0);
    }
    public GridPane getView(){
        return view;
    }
}
        


