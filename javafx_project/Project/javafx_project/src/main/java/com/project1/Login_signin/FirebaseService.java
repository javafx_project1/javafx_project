package com.project1.Login_signin;

import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;

import org.json.JSONObject;


import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthException;
import com.google.firebase.auth.UserRecord;
import com.project1.connectionpage.ConnectionPage;

import javafx.scene.control.Alert;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;

public class FirebaseService {
    private TextField emailField;
    private PasswordField passwordField;
    public FirebaseService(LoginPage loginPage, TextField emailField, PasswordField passwordField){
        this.emailField = emailField;
        this.passwordField = passwordField;
    }
    public FirebaseService(LoginPage app){
       
    }

    public boolean signUp(){
        String email = emailField.getText();
        String passWord = passwordField.getText();

        try{

            UserRecord.CreateRequest request = new UserRecord.CreateRequest().setEmail(email).setPassword(passWord).setDisabled(false);

            UserRecord UserRecord = FirebaseAuth.getInstance().createUser(request);
            System.out.println("Successfully created user: "+ UserRecord.getUid());
            showAleart("Success", "User created successfully.");
            return true;
        }catch(FirebaseAuthException e){
            e.printStackTrace();
            showAleart("Error", "Failed to create user: " + e.getMessage());
            return false;
        }
    }

    public boolean login(){
        String email = emailField.getText();
        String password = passwordField.getText();

        try{
            String apikey = "AIzaSyDD4jsb4fi292-2cbBcJWlL-CKgLU6u5xU";
            URL url = new URL("https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=" + apikey);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/json; charset-UTF-8");
            conn.setDoOutput(true);

            JSONObject jsonRequest = new JSONObject();
            jsonRequest.put("email", email);
            jsonRequest.put("password", password);
            jsonRequest.put("returnSecureToken", true);

            try(OutputStream os = conn.getOutputStream()){
                byte[] input = jsonRequest.toString().getBytes(StandardCharsets.UTF_8);
                os.write(input, 0, input.length);
            }

            if(conn.getResponseCode() == 200){
                showlogin(false);
                return true;
            } else{
                showAleart("Invalid Login", "Invalid credintial!!");
                return false;
            }
        } catch(Exception e){
            e.printStackTrace();
            showlogin(false);
            return false;
        }
    }

    
    private void showAleart(String title, String message){
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle(title);
        alert.setHeaderText(null);
        alert.setContentText(message);
        alert.showAndWait();
    }

    private void showlogin(boolean Log){
        ConnectionPage.Home1();
    }

    
}
