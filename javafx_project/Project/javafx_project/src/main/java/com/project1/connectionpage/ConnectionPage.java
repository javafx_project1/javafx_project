package com.project1.connectionpage;

import com.project1.BookStrore.Bookstorepage1;
import com.project1.BookStrore.cart;
import com.project1.Homepage.homePage;
import com.project1.Homepage.deaseasePage.Deasease;
import com.project1.Homepage.deaseasePage.Deseases.Cancer1;
import com.project1.Homepage.drugsPage.Drugs;
import com.project1.Homepage.drugsPage.Druges.Pil1;
import com.project1.Login_signin.AfterLogin;
import com.project1.Login_signin.LoginPage;
import com.project1.Quiz.QuizhomePage;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class ConnectionPage extends Application{
    private static Stage prStage;
    private static Scene homepageScane;
    private Scene deaseaseScene;
    private Scene loginScene;
    private Scene bookstScenesScene;
    private Scene quizhomeScene;
    private Scene cartScene;
    private Scene Cancer1Scene;

    private Scene drugScene;
    private Scene pil1Scene;
    private static Scene AfterlogScene;

    private homePage homePage1;

    private Deasease deasease;
    private Cancer1 cancer1;

    private Drugs  drugs;
    private Pil1 pil1;
    

    private LoginPage loginPage;
    private AfterLogin AfterlogPage;
    private Bookstorepage1 bookStore;
    private QuizhomePage Quiz;
    private cart cartpage;

    @Override
    public void start(Stage prStage) throws Exception {

        prStage.setTitle("Medical Guide");
        ConnectionPage.prStage = prStage;
        homePage1 = new homePage(this);
        deasease = new Deasease(this);
        cancer1 = new Cancer1(this);
        drugs = new Drugs(this);
        pil1 = new Pil1(this);
        loginPage = new LoginPage(this);
        AfterlogPage = new AfterLogin(this);
        bookStore = new Bookstorepage1(this);
        Quiz = new QuizhomePage(this);
        cartpage = new cart(this);



        homepageScane = new Scene(homePage1.getView(),1700, 1000);
        deaseaseScene = new Scene(deasease.getView(), 1700, 1000);
        Cancer1Scene = new Scene(cancer1.getView(), 1700, 1200);
        drugScene = new Scene(drugs.getView(), 1700, 1000);
        pil1Scene = new Scene(pil1.getView(), 1700, 2100);
        loginScene = new Scene(loginPage.getView(), 1700, 1000);
        
        AfterlogScene = new Scene(AfterlogPage.getView(), 1700, 1000);
        bookstScenesScene = new Scene(bookStore.getView(), 1700,1000);
        quizhomeScene = new Scene(Quiz.getView(), 1700, 1000);
        cartScene = new Scene(cartpage.getView(), 1700, 1000);
        prStage.setScene(bookstScenesScene);
        prStage.show();
        
    }
    public static void Home1(){
        prStage.setScene(homepageScane);
    }
    public void Login1(){
        prStage.setScene(loginScene);
    }
    public static void Afterlog1(){
        prStage.setScene(AfterlogScene);
    }
    public void bookstrore(){
        prStage.setScene(bookstScenesScene);
    }

    public void Quizhome1(){
        prStage.setScene(quizhomeScene);
    }

    public void Cart1(){
        prStage.setScene(cartScene);
    }
    public void Deseasese(){
        prStage.setScene(deaseaseScene);
    }
    public void Cancer1(){
        prStage.setScene(Cancer1Scene);
    }
    public void Drugs(){
        prStage.setScene(drugScene);
    }
    public void Pil1(){
        prStage.setScene(pil1Scene);
    }

    
    
}
