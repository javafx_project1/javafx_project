package com.project1.Login_signin;

import java.io.FileInputStream;
import java.io.IOException;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.project1.connectionpage.ConnectionPage;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;

public class LoginPage {
    private ConnectionPage app;
    private GridPane view;
    private FirebaseService firebaseService;

    public LoginPage(ConnectionPage app){
        this.app = app;
        initialize();
    }

    public void initialize(){

         try{
            FileInputStream serviceAccount = new FileInputStream("javafx_project/src/main/resources/fx-my-authen.json");

            @SuppressWarnings("deprecation")
            FirebaseOptions options = new FirebaseOptions.Builder()
                .setCredentials(GoogleCredentials.fromStream(serviceAccount))
                .setDatabaseUrl("https://fx-auth-fb-a1621-default-rtdb.asia-southeast1.firebasedatabase.app/")
                .build();
            FirebaseApp.initializeApp(options);
        } catch(IOException e){
            e.printStackTrace();

        }

        view = new GridPane();

        // Set background image
        // view.setStyle("-fx-background-image: url('abstrac.jpg'); " +
        //               "-fx-background-size: cover; " +
        //               "-fx-background-position: center center;"+"-fx-border-color:black");

        Label well = new Label("Wellcome!");
        well.setFont(new Font(30));
        HBox wellBox = new HBox(well);
        wellBox.setMaxWidth(300);
        wellBox.setMinHeight(100);
        wellBox.setAlignment(Pos.TOP_CENTER);


        Label UserName = new Label("User Name");
        HBox userBox = new HBox(UserName);
        userBox.setMaxWidth(335);
        userBox.setAlignment(Pos.CENTER_LEFT);
        
        TextField emailField = new TextField();
        emailField.setMaxWidth(350);
        //emailField.setAlignment(Pos.CENTER);
        //UserName.setAlignment(Pos.CENTER);

        VBox userVBox = new VBox(10, userBox, emailField);
        userVBox.setMaxWidth(350);
        
        Label Pass = new Label("Password:");
        HBox passBox = new HBox(Pass);
        passBox.setMaxWidth(335);
        passBox.setAlignment(Pos.CENTER_LEFT);
        Pass.setAlignment(Pos.CENTER);
        PasswordField passwordField = new PasswordField();
        //passwordField.setMaxWidth(350);
        //passwordField.setAlignment(Pos.CENTER);
        Label lb = new Label();

        VBox passVBox = new VBox(10, passBox, passwordField);
        passVBox.setMaxWidth(350);

        firebaseService = new FirebaseService(this, emailField, passwordField);

        Button submit = new Button("Login");
        submit.setPrefWidth(100);
        submit.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
               firebaseService.login();
                
            }
            
        });

        Button signbutton = new Button("Sign Up");
        signbutton.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                firebaseService.signUp();
                
            }
            
        });

        HBox subhb = new HBox(20, submit, signbutton);
        subhb.setPrefWidth(30);
        subhb.setPrefHeight(50);
        subhb.setAlignment(Pos.CENTER);


        VBox loginVBox = new VBox(20,wellBox,userVBox, passVBox, subhb);
        loginVBox.setMaxWidth(470);
        loginVBox.setPrefHeight(600);
        loginVBox.setAlignment(Pos.CENTER);
        loginVBox.setStyle("-fx-background-radius: 20px;-fx-background-color:#F2FFFFFF; -fx-hgap: 20px; -fx-padding: 40px; -fx-border-radius: 20px; -fx-effect: dropshadow(three-pass-box, rgba(0, 0, 0, 8), 5, 0, 0, 0);");


        
        VBox vb3 = new VBox(loginVBox);
        vb3.setPrefSize(1700, 1000);
        vb3.setStyle("-fx-background-image: url('file:Project/javafx_project/src/main/resources/wallpaper/image4.jpg'); ");
        //vb3.setStyle("-fx-background-color:yellow");
        vb3.setAlignment(Pos.CENTER);

        HBox hb = new HBox(vb3);

        view.add(hb, 0, 0);
        view.setAlignment(Pos.CENTER);
        
    }

    public GridPane getView(){
        return view;
    }
}

