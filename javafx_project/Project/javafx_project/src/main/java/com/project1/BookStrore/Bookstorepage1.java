package com.project1.BookStrore;

import com.project1.connectionpage.ConnectionPage;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.ScrollPane.ScrollBarPolicy;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.shape.Circle;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;

public class Bookstorepage1 {

    private ConnectionPage app;
    private GridPane view;


    public Bookstorepage1(ConnectionPage app){
        this.app=app;
        initialize();
    }

    private void initialize(){
        Button cartButton = new Button("Cart");
        cartButton.setPrefWidth(130);
        cartButton.setPrefHeight(40);
        cartButton.setFont(Font.font("", FontWeight.BOLD, 15));
        cartButton.setStyle("-fx-background-color:#3DED97");
        cartButton.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                app.Cart1();
            }
            
        });
        view = new GridPane();

        Image image = new Image("file:C:\\Java_Progect\\javafx_project\\Project\\javafx_project\\src\\main\\resources\\wallpaper.jpg");
        ImageView imageVi = new ImageView(image);
        imageVi.setFitWidth(1700);
        imageVi.setFitHeight(1000);
       
        view.getChildren().add(imageVi);

        Button logoutButton = new Button("Logout");
        logoutButton.setPrefWidth(130);
        logoutButton.setPrefHeight(40);
        logoutButton.setFont(Font.font("", FontWeight.BOLD, 15));
        logoutButton.setStyle("-fx-background-color:#FF0800");
        
        HBox hb1 = new HBox();
        hb1.setPrefHeight(10);
        hb1.setPrefWidth(1700);
        hb1.setStyle("-fx-background-color:transperant");

       Image ig = new Image("file:C:/Java_Progect/javafx_project/Project/javafx_project/src/main/resources/Medical Book.png");
       ImageView iv = new ImageView(ig);
       iv.setPreserveRatio(true);


        Button logo = new Button();
        Circle circle = new Circle(0.5); 
        logo.setShape(circle);
        logo.setPrefWidth(100);
        logo.setPrefHeight(50);
        double buttonWidth = 100;
        double buttonHeight = 70;
        iv.setFitWidth( buttonWidth);
        iv.setFitHeight( buttonHeight);
        iv.setPreserveRatio(true);
        logo.setGraphic(iv);
        logo.setMinSize(100, 70);
        logo.setMaxSize(100, 70);
        logo.setStyle("-fx-background-color: transparent;");

        Label name = new Label("Medical Guide");
        name.setFont(Font.font("", FontWeight.BOLD, 30));
        //name.setFont(new Font(30));
        //name.setStyle("-fx-background-color:white");
        name.setAlignment(Pos.CENTER);

        HBox hb3 = new HBox(10,logo,name);     
        hb3.setPrefWidth(1500);
        hb3.setPrefHeight(100);
        hb3.setAlignment(Pos.CENTER_LEFT);

       



        HBox hb4 = new HBox(40,cartButton,logoutButton);
        hb4.setPrefWidth(500);
        hb4.setPrefHeight(100);
        hb4.setAlignment(Pos.CENTER);

        HBox hb2 = new HBox(hb3,hb4);
        hb2.setPrefHeight(100);
        hb2.setPrefWidth(1700);
        //hb2.setLayoutY(70);
        hb2.setStyle("-fx-background-color:transperant");

        //Image homeimg = new Image("home.jpeg");
        //ImageView imgView1 = new ImageView(homeimg);
        Button home = new Button("Home");
        home.setPrefWidth(100);
        home.setStyle("-fx-background-color:transparent");
        //home.setTextFill(Color.WHITE);
        home.setFont(Font.font("", FontWeight.BOLD, 22));
        home.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                app.Home1();
            }
            
        });

        Button BookStore = new Button("Book Store");
        BookStore.setPrefWidth(150);
        BookStore.setStyle("-fx-background-color:transparent");
        //BookStore.setTextFill(Color.WHITE);
        BookStore.setFont(Font.font("", FontWeight.BOLD, 22));
        BookStore.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                app.bookstrore();
            }
            
        });

        


        Button Quiz = new Button("Quiz");
        Quiz.setPrefWidth(100);
        Quiz.setStyle("-fx-background-color:transparent");
        //Quiz.setTextFill(Color.WHITE);
        Quiz.setFont(Font.font("", FontWeight.BOLD, 22));

        Quiz.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                app.Quizhome1();
            }
            
        });

        HBox hb6 = new HBox(30,home, BookStore, Quiz);
        hb6.setPrefHeight(60);
        hb6.setPrefWidth(700);
        hb6.setAlignment(Pos.CENTER_LEFT);
        

        Image image1 = new Image("menu.png");
        ImageView imageView = new ImageView(image1);
        imageView.setFitWidth(20);
        imageView.setFitHeight(20);
        imageView.setPreserveRatio(false);
        Button setting = new Button();
        setting.setGraphic(imageView);
        //setting.setPrefWidth(120);
        //setting.setPrefHeight(40);
        

        HBox hb7 = new HBox(setting);
        hb7.setPrefHeight(60);
        hb7.setPrefWidth(960);
        hb7.setAlignment(Pos.CENTER_RIGHT);

        

        HBox hb5 = new HBox(hb6, hb7);
        hb5.setPrefWidth(1700);
        hb5.setPrefHeight(60);
        hb5.setStyle("-fx-background-color:white");

     
        
       

        VBox vb1 = new VBox(hb1, hb2, hb5);
        //vb1.setStyle("-fx-background-color:yellow");


     //D-----------------------------------------------------------------------------------------------------   

        // add  book button or label here bbox1
        Image ig1 = new Image("file:Project/javafx_project/src/main/resources/wallpaper/book_ig1.jpeg");
        ImageView iv1 = new ImageView(ig1);

        Button b1 = new Button();
        b1.setPrefSize(170, 200);
        b1.setGraphic(iv1);

        Button add1 = new Button("Add to card");

        VBox book1 = new VBox(10, b1, add1);
        book1.setAlignment(Pos.CENTER);

        Image ig2 = new Image("file:Project/javafx_project/src/main/resources/wallpaper/book_ig1.jpeg");
        ImageView iv2 = new ImageView(ig2);

        Button b2 = new Button();
        b2.setPrefSize(170, 200);
        b2.setGraphic(iv2);

        Button add2 = new Button("Add to card");

        VBox book2 = new VBox(10, b2, add2);
        book2.setAlignment(Pos.CENTER);

        Image ig3 = new Image("file:Project/javafx_project/src/main/resources/wallpaper/book_ig1.jpeg");
        ImageView iv3 = new ImageView(ig3);

        Button b3 = new Button();
        b3.setPrefSize(170, 200);
        b3.setGraphic(iv3);
        Button add3 = new Button("Add to card");

        VBox book3 = new VBox(10, b3, add3);
        book3.setAlignment(Pos.CENTER);

        Image ig4 = new Image("file:Project/javafx_project/src/main/resources/wallpaper/book_ig1.jpeg");
        ImageView iv4 = new ImageView(ig4);

        Button b4 = new Button();
        b4.setPrefSize(170, 200);
        b4.setGraphic(iv4);
        Button add4 = new Button("Add to card");

        VBox book4 = new VBox(10, b4, add4);
        book4.setAlignment(Pos.CENTER);


        HBox BBox1 = new HBox(80, book1, book2, book3, book4);
        BBox1.setPrefHeight(330);
        BBox1.setPrefWidth(1500);
        BBox1.setAlignment(Pos.CENTER);
        // BBox1.setStyle(
        //     "-fx-background-image: url('file:C:/Java_Progect/javafx_project/Project/javafx_project/src/main/resources/wallpaper/wall.jpg');" +
        //     "-fx-background-size: cover;" +
        //     "-fx-background-position: center center;"
        // );

        HBox hBox1 = new HBox(BBox1);
        hBox1.setAlignment(Pos.CENTER);
        hBox1.setMaxWidth(1500);
        hBox1.setPrefHeight(350);
        // hBox1.setStyle(
        //     "-fx-background-image: url('file:C:/Java_Progect/javafx_project/Project/javafx_project/src/main/resources/wallpaper/wall.jpg');" +
        //     "-fx-background-size: cover;" +
        //     "-fx-background-position: center center;"
        // );
       
        //hBox1.setStyle("-fx-border-color:black");




        //Book block 2-----------------------------------------------------------------------------------------------------
        
        // add second book h Box buttons in bbox2
        Image b2ig1 = new Image("file:Project/javafx_project/src/main/resources/wallpaper/book_ig1.jpeg");
        ImageView b2iv1 = new ImageView(b2ig1);

        Button b2b1 = new Button();
        b2b1.setPrefSize(170, 200);
        b2b1.setGraphic(b2iv1);

        Button b2add1 = new Button("Add to card");

        VBox b2book1 = new VBox(10, b2b1, b2add1);
        b2book1.setAlignment(Pos.CENTER);

        Image b2ig2 = new Image("file:Project/javafx_project/src/main/resources/wallpaper/book_ig1.jpeg");
        ImageView b2iv2 = new ImageView(b2ig2);

        Button b2b2 = new Button();
        b2b2.setPrefSize(170, 200);
        b2b2.setGraphic(b2iv2);

        Button b2add2 = new Button("Add to card");

        VBox b2book2 = new VBox(10, b2b2, b2add2);
        b2book2.setAlignment(Pos.CENTER);

        Image b2ig3 = new Image("file:Project/javafx_project/src/main/resources/wallpaper/book_ig1.jpeg");
        ImageView b2iv3 = new ImageView(b2ig3);

        Button b2b3 = new Button();
        b2b3.setPrefSize(170, 200);
        b2b3.setGraphic(b2iv3);
        Button b2add3 = new Button("Add to card");

        VBox b2book3 = new VBox(10, b2b3, b2add3);
        b2book3.setAlignment(Pos.CENTER);

        Image b2ig4 = new Image("file:Project/javafx_project/src/main/resources/wallpaper/book_ig1.jpeg");
        ImageView b2iv4 = new ImageView(b2ig4);

        Button b2b4 = new Button();
        b2b4.setPrefSize(170, 200);
        b2b4.setGraphic(b2iv4);
        Button b2add4 = new Button("Add to card");

        VBox b2book4 = new VBox(10, b2b4, b2add4);
        b2book4.setAlignment(Pos.CENTER);
        

        HBox BBox2 = new HBox(80, b2book1, b2book2, b2book3, b2book4);
        BBox2.setPrefWidth(1500);
        BBox2.setPrefHeight(330);
        BBox2.setAlignment(Pos.CENTER);
        // BBox2.setStyle(
        //     "-fx-background-image: url('file:C:/Java_Progect/javafx_project/Project/javafx_project/src/main/resources/wallpaper/wall.jpg');" +
        //     "-fx-background-size: cover;" +
        //     "-fx-background-position: center center;"
        // );

        HBox hBox2 = new HBox(BBox2);
        hBox2.setPrefHeight(350);
        hBox2.setPrefWidth(1500);
        hBox2.setAlignment(Pos.CENTER);
        // hBox2.setStyle(
        //     "-fx-background-image: url('file:C:/Java_Progect/javafx_project/Project/javafx_project/src/main/resources/wallpaper/wall.jpg');" +
        //     "-fx-background-size: cover;" +
        //     "-fx-background-position: center center;"
        // );
        //hBox2.setStyle("-fx-background-color:transparent");
//---------------Book block3---------------------------------------------------------------------------------------------------
        HBox hBox = new HBox();
        hBox.setPrefHeight(20);
        hBox.setPrefWidth(1700);
        hBox.setAlignment(Pos.CENTER);


        // add books3 in bbox3 hbox

        HBox BBox3 = new HBox(30);
        BBox3.setPrefWidth(1500);
        BBox3.setPrefHeight(330);
        // BBox3.setStyle(
        //     "-fx-background-image: url('file:C:/Java_Progect/javafx_project/Project/javafx_project/src/main/resources/wallpaper/wall.jpg');" +
        //     "-fx-background-size: cover;" +
        //     "-fx-background-position: center center;"
        // );

        HBox hBox3 = new HBox(BBox3);
        hBox3.setPrefHeight(350);
        hBox3.setPrefWidth(1500);
        hBox3.setAlignment(Pos.CENTER);
        // hBox3.setStyle(
        //     "-fx-background-image: url('file:C:/Java_Progect/javafx_project/Project/javafx_project/src/main/resources/wallpaper/wall.jpg');" +
        //     "-fx-background-size: cover;" +
        //     "-fx-background-position: center center;"
        // );
        //hBox3.setStyle("-fx-background-color:transparent");

        // add bokks 4 in bb4 hox

        HBox BBox4 = new HBox(30);
        BBox4.setPrefWidth(1500);
        BBox4.setPrefHeight(330);
        // BBox4.setStyle(
        //     "-fx-background-image: url('file:C:/Java_Progect/javafx_project/Project/javafx_project/src/main/resources/wallpaper/wall.jpg');" +
        //     "-fx-background-size: cover;" +
        //     "-fx-background-position: center center;"
        // );

        HBox hBox4 = new HBox(BBox4);
        hBox4.setPrefHeight(350);
        hBox4.setPrefWidth(1500);
        hBox4.setAlignment(Pos.CENTER);
        // hBox4.setStyle(
        //     "-fx-background-image: url('file:C:/Java_Progect/javafx_project/Project/javafx_project/src/main/resources/wallpaper/wall.jpg');" +
        //     "-fx-background-size: cover;" +
        //     "-fx-background-position: center center;"
        // );
        //hBox4.setStyle("-fx-background-color:transparent");




        VBox vb2 = new VBox(60,hBox,hBox1,hBox2,hBox3,hBox4);
        //vb2.setPrefWidth(1700);
        hBox1.setAlignment(Pos.CENTER);
        hBox2.setAlignment(Pos.CENTER);
       // PhBox.setAlignment(Pos.CENTER);
        vb2.setPrefHeight(2100);
    //     vb2.setStyle(
    //         "-fx-background-image: url('file:C:/Java_Progect/javafx_project/Project/javafx_project/src/main/resources/wallpaper/wall.jpg');" +
    //         "-fx-background-size: cover;" +
    //         "-fx-background-position: center center;"
    // );
        // vb2.setStyle("-fx-background-color:transparent");
        vb2.setAlignment(Pos.TOP_CENTER);

        ScrollPane dpcroll = new ScrollPane();
        dpcroll.setVbarPolicy(ScrollBarPolicy.ALWAYS);
        dpcroll.setHbarPolicy(ScrollBarPolicy.NEVER);
        dpcroll.setContent(vb2);
        dpcroll.setMaxHeight(2100);
        //dpcroll.setStyle("-fx-border-color:transperant");

        VBox vb3 = new VBox(vb1,vb2,dpcroll);
        
        //  vb3.setStyle("-fx-background:src/main/resources/Medical Book.png");
        vb3.setAlignment(Pos.CENTER);
        view.add(vb3, 0, 0);
        view.setStyle("-fx-background-image: url('file:Project/javafx_project/src/main/resources/wallpaper/image4.jpg'); ");
       
       




       
    }

    public GridPane getView(){
        return view;
    }
    
}