package com.project1.Homepage;

import com.project1.connectionpage.ConnectionPage;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.ScrollPane.ScrollBarPolicy;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.shape.Circle;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;

public class homePage {

    private ConnectionPage app;
    private GridPane view;


    public homePage(ConnectionPage app){
        this.app=app;
        initialize();
    }

    private void initialize(){

        Button cartButton = new Button("Cart");
        cartButton.setPrefWidth(130);
        cartButton.setPrefHeight(40);
        cartButton.setFont(Font.font("", FontWeight.BOLD, 15));
        //LoginButton.setTextFill(Color.WHITE);
        cartButton.setStyle("-fx-background-color:#3DED97");
        
        
        
        cartButton.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                app.Cart1();
            }
            
        });
        view = new GridPane();

        Image image = new Image("file:C:\\Java_Progect\\javafx_project\\Project\\javafx_project\\src\\main\\resources\\wallpaper.jpg");
        ImageView imageVi = new ImageView(image);
        imageVi.setFitWidth(1700);
        imageVi.setFitHeight(1000);
       
        view.getChildren().add(imageVi);

        Button logoutButton = new Button("Logout");
        logoutButton.setPrefWidth(130);
        logoutButton.setPrefHeight(40);
        logoutButton.setFont(Font.font("", FontWeight.BOLD, 15));
        logoutButton.setStyle("-fx-background-color:#FF0800");
        logoutButton.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                app.Login1();
            }
            
        });
        
        HBox hb1 = new HBox();
        hb1.setPrefHeight(10);
        hb1.setPrefWidth(1700);
        hb1.setStyle("-fx-background-color:transperant");

       Image ig = new Image("file:Project/javafx_project/src/main/resources/Medical Book.png");
       ImageView iv = new ImageView(ig);
       iv.setPreserveRatio(true);


        Button logo = new Button();
        Circle circle = new Circle(0.5); 
        logo.setShape(circle);
        logo.setPrefWidth(100);
        logo.setPrefHeight(50);
        double buttonWidth = 100;
        double buttonHeight = 70;
        iv.setFitWidth( buttonWidth);
        iv.setFitHeight( buttonHeight);
        iv.setPreserveRatio(true);
        logo.setGraphic(iv);
        logo.setMinSize(100, 70);
        logo.setMaxSize(100, 70);
        logo.setStyle("-fx-background-color: transparent;");

        Label name = new Label("Medical Guide");
        name.setFont(Font.font("", FontWeight.BOLD, 30));
        //name.setFont(new Font(30));
        //name.setStyle("-fx-background-color:white");
        name.setAlignment(Pos.CENTER);

        HBox hb3 = new HBox(10,logo,name);     
        hb3.setPrefWidth(1500);
        hb3.setPrefHeight(100);
        hb3.setAlignment(Pos.CENTER_LEFT);

       



        HBox hb4 = new HBox(40,cartButton,logoutButton);
        hb4.setPrefWidth(500);
        hb4.setPrefHeight(100);
        hb4.setAlignment(Pos.CENTER);

        HBox hb2 = new HBox(hb3,hb4);
        hb2.setPrefHeight(100);
        hb2.setPrefWidth(1700);
        //hb2.setLayoutY(70);
        hb2.setStyle("-fx-background-color:transperant");

        //Image homeimg = new Image("home.jpeg");
        //ImageView imgView1 = new ImageView(homeimg);
        Button home = new Button("Home");
        home.setPrefWidth(100);
        home.setStyle("-fx-background-color:transparent");
        //home.setTextFill(Color.WHITE);
        home.setFont(Font.font("", FontWeight.BOLD, 22));
        Button BookStore = new Button("Book Store");
        BookStore.setPrefWidth(150);
        BookStore.setStyle("-fx-background-color:transparent");
        //BookStore.setTextFill(Color.WHITE);
        BookStore.setFont(Font.font("", FontWeight.BOLD, 22));

        BookStore.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                app.bookstrore();
            }
            
        });


        Button Quiz = new Button("Quiz");
        Quiz.setPrefWidth(100);
        Quiz.setStyle("-fx-background-color:transparent");
        //Quiz.setTextFill(Color.WHITE);
        Quiz.setFont(Font.font("", FontWeight.BOLD, 22));

        Quiz.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                app.Quizhome1();
            }
            
        });

        HBox hb6 = new HBox(30,home, BookStore, Quiz);
        hb6.setPrefHeight(60);
        hb6.setPrefWidth(700);
        hb6.setAlignment(Pos.CENTER_LEFT);
        

        Image image1 = new Image("menu.png");
        ImageView imageView = new ImageView(image1);
        imageView.setFitWidth(20);
        imageView.setFitHeight(20);
        imageView.setPreserveRatio(false);
        Button setting = new Button();
        setting.setGraphic(imageView);
        //setting.setPrefWidth(120);
        //setting.setPrefHeight(40);
        

        HBox hb7 = new HBox(setting);
        hb7.setPrefHeight(60);
        hb7.setPrefWidth(960);
        hb7.setAlignment(Pos.CENTER_RIGHT);

        

        HBox hb5 = new HBox(hb6, hb7);
        hb5.setPrefWidth(1700);
        hb5.setPrefHeight(60);
        hb5.setStyle("-fx-background-color:white");

       // hb5.setAlignment(Pos.CENTER);

        // Image image = new Image("back.avif");
        // ImageView ig = new ImageView(image);

        // Button seeallbtn = new Button("See All");
        // HBox d1 = new HBox(seeallbtn);
        
        // d1.setPrefWidth(1920);
        // d1.setPrefHeight(20);
        // d1.setAlignment(Pos.CENTER_RIGHT);
        //d1.setLayoutY(300);
        //d1.setStyle("-fx-background-color:black");

        //ScrollBar1
        
       

        VBox vb1 = new VBox(hb1, hb2, hb5);
        //vb1.setStyle("-fx-background-color:yellow");
     //D-----------------------------------------------------------------------------------------------------   

        Button button = new Button("dese");
        button.setPrefHeight(250);
        button.setPrefWidth(200);


        Button button1 = new Button("dese");
        button1.setPrefHeight(300);
        button1.setPrefWidth(250);

        Button button2 = new Button("dese");
        button2.setPrefHeight(300);
        button2.setPrefWidth(250);

        
        Button button3 = new Button("dese");
        button3.setPrefHeight(300);
        button3.setPrefWidth(250);

        
        Button button4 = new Button("dese");
        button4.setPrefHeight(300);
        button4.setPrefWidth(250);

        Button button5 = new Button("dese");
        button5.setPrefHeight(300);
        button5.setPrefWidth(250);

        Button button6 = new Button("dese");
        button6.setPrefHeight(300);
        button6.setPrefWidth(250);
        
        Button buttonl = new Button("see all");
        buttonl.setPrefHeight(50);
        buttonl.setPrefWidth(100);
        buttonl.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
               app.Deseasese();
            }
            
        });


       

        HBox DBox = new HBox(30,button,button1,button2,button3,button4,button5,button6,buttonl);
        DBox.setPrefHeight(330);
        DBox.setPrefWidth(2100);
        // DBox.setStyle(
        //     "-fx-background-image: url('file:C:/Java_Progect/javafx_project/Project/javafx_project/src/main/resources/wallpaper/wall.jpg');" +
        //     "-fx-background-size: cover;" +
        //     "-fx-background-position: center center;"
        // );

        ScrollPane Dscorll = new ScrollPane();
        Dscorll.setContent(DBox);
        Dscorll.setHbarPolicy(ScrollBarPolicy.ALWAYS);
        Dscorll.setVbarPolicy(ScrollBarPolicy.NEVER);
        Dscorll.setMaxWidth(1500);
        //Dscorll.setStyle("-fx-background-color:#02A0F9");
        //Dscorll.setStyle("-fx-background-color:#02A0F9");
        
    
        HBox DhBox = new HBox(DBox,Dscorll);
        DBox.setAlignment(Pos.CENTER);
        DhBox.setPrefWidth(1700);
        DhBox.setPrefHeight(350);
        // DhBox.setStyle(
        //     "-fx-background-image: url('file:C:/Java_Progect/javafx_project/Project/javafx_project/src/main/resources/wallpaper/wall.jpg');" +
        //     "-fx-background-size: cover;" +
        //     "-fx-background-position: center center;"
        // );
       
        //DhBox.setStyle("-fx-background-color:transparent");




        //P-----------------------------------------------------------------------------------------------------
        
        Button buttonp1 = new Button("pill");
        buttonp1.setPrefHeight(300);
        buttonp1.setPrefWidth(250);


        Button buttonp2 = new Button("pil");
        buttonp2.setPrefHeight(300);
        buttonp2.setPrefWidth(250);

        Button buttonp3 = new Button("pil");
        buttonp3.setPrefHeight(300);
        buttonp3.setPrefWidth(250);

        
        Button buttonp4 = new Button("pil");
        buttonp4.setPrefHeight(300);
        buttonp4.setPrefWidth(250);

        
        Button buttonp5 = new Button("pil");
        buttonp5.setPrefHeight(300);
        buttonp5.setPrefWidth(250);

        Button buttonp6 = new Button("pil");
        buttonp6.setPrefHeight(300);
        buttonp6.setPrefWidth(250);

        Button buttonp7 = new Button("pil");
        buttonp7.setPrefHeight(300);
        buttonp7.setPrefWidth(250);
        
        Button buttonpl = new Button("see all");
        buttonpl.setPrefHeight(50);
        buttonpl.setPrefWidth(100);
        buttonpl.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
               app.Drugs();
            }
            
        });


        HBox PBox = new HBox(30,buttonp1,buttonp2,buttonp3,buttonp4,buttonp5,buttonp6,buttonp7,buttonpl);
        PBox.setPrefWidth(2100);
        PBox.setPrefHeight(330);
        // PBox.setStyle("-fx-background-color:white");
       // PBox.setStyle("-fx-border-color:black");
        // PBox.setStyle(
        //     "-fx-background-image: url('file:C:/Java_Progect/javafx_project/Project/javafx_project/src/main/resources/wallpaper/wall.jpg');" +
        //     "-fx-background-size: cover;" +
        //     "-fx-background-position: center center;"
        // );
        //PBox.setStyle("-fx-background-color:transperant");
        
        ScrollPane Pscroll = new ScrollPane();
        Pscroll.setHbarPolicy(ScrollBarPolicy.ALWAYS);
        Pscroll.setVbarPolicy(ScrollBarPolicy.NEVER);
        Pscroll.setContent(PBox);
        Pscroll.setMaxWidth(1500);
       // Pscroll.setStyle("-fx-border-color:transperant");

        HBox PhBox = new HBox(PBox,Pscroll);
        PhBox.setPrefHeight(350);
        PhBox.setPrefWidth(1700);
        //PhBox.setStyle("-fx-border-color:green");
        PhBox.setAlignment(Pos.CENTER);
        PhBox.setStyle(
            "-fx-background-image: url('file:C:/Java_Progect/javafx_project/Project/javafx_project/src/main/resources/wallpaper/wall.jpg');" +
            "-fx-background-size: cover;" +
            "-fx-background-position: center center;"
    );
        //PhBox.setStyle("-fx-background-color:transparent");

        HBox hBox = new HBox();
        hBox.setPrefHeight(20);
        hBox.setPrefWidth(1700);
        hBox.setAlignment(Pos.CENTER);



        VBox vb2 = new VBox(60,hBox,DhBox,PhBox);
        //vb2.setStyle("-fx-background-image: url('file:Project/javafx_project/src/main/resources/wallpaper/image4.jpg'); ");
        //vb2.setPrefWidth(1700);
        PBox.setAlignment(Pos.CENTER);
        DhBox.setAlignment(Pos.CENTER);
       // PhBox.setAlignment(Pos.CENTER);
        vb2.setPrefHeight(1000);
    //     vb2.setStyle(
    //         "-fx-background-image: url('file:C:/Java_Progect/javafx_project/Project/javafx_project/src/main/resources/wallpaper/wall.jpg');" +
    //         "-fx-background-size: cover;" +
    //         "-fx-background-position: center center;"
    // );
        //vb2.setStyle("-fx-background-color:transparent");
        vb2.setAlignment(Pos.TOP_CENTER);

        ScrollPane dpcroll = new ScrollPane();
        dpcroll.setHbarPolicy(ScrollBarPolicy.ALWAYS);
        dpcroll.setVbarPolicy(ScrollBarPolicy.NEVER);
        dpcroll.setContent(vb2);
        dpcroll.setMaxHeight(1200);
        //dpcroll.setStyle("-fx-border-color:transperant");

        VBox vb3 = new VBox(vb1,vb2,dpcroll);
        vb3.setStyle("-fx-background-image: url('file:Project/javafx_project/src/main/resources/wallpaper/image4.jpg'); ");
        VBox vb4Box = new VBox(vb3);
        //vb4Box.setStyle("-fx-background-image: url('file:Project/javafx_project/src/main/resources/wallpaper/image4.jpg'); ");
        
        //  vb3.setStyle("-fx-background:src/main/resources/Medical Book.png");
        vb3.setAlignment(Pos.CENTER);
        view.add(vb4Box, 0, 0);
       
       



    }

    public GridPane getView(){
        return view;
    }
    
}
