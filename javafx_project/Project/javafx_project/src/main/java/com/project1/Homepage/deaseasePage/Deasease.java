package com.project1.Homepage.deaseasePage;


import com.project1.connectionpage.ConnectionPage;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.control.Button;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.ScrollPane.ScrollBarPolicy;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

public class Deasease {
    private ConnectionPage app;
    
    private static GridPane view;

    public Deasease(ConnectionPage app) {
        this.app = app;
        initialize();
    }


    public void initialize() {

        view = new GridPane();
       
        Button back = new Button("back");
        back.setPrefWidth(100);
        back.setAlignment(Pos.CENTER);
        back.setStyle("-fx-background-color:White");
        back.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
               app.Home1();
            }
            
        });

        HBox hBox = new HBox(10,back);
        hBox.setPrefHeight(100);
        hBox.setPrefWidth(1700); 
        hBox.setAlignment(Pos.CENTER_LEFT);
        hBox.setStyle("-fx-background-color:Blue");


        Button bt1 = new Button("caner1");
        bt1.setPrefHeight(300);
        bt1.setPrefWidth(250);
        bt1.setStyle("-fx-background-color:aqua");
        bt1.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
               app.Cancer1();
            }
            
        });

        Button bt2 = new Button("caner");
        bt2.setPrefHeight(300);
        bt2.setPrefWidth(250);
        bt2.setStyle("-fx-background-color:aqua");

        Button bt3 = new Button("caner");
        bt3.setPrefHeight(300);
        bt3.setPrefWidth(250);
        bt3.setStyle("-fx-background-color:aqua");

        Button bt4 = new Button("caner");
        bt4.setPrefHeight(300);
        bt4.setPrefWidth(250);
        bt4.setStyle("-fx-background-color:aqua");

        Button bt5 = new Button("caner");
        bt5.setPrefHeight(300);
        bt5.setPrefWidth(250);
        bt5.setStyle("-fx-background-color:aqua");


        VBox vbe = new VBox();
        vbe.setPrefHeight(300);
        vbe.setPrefWidth(110);

        HBox hBox2 = new HBox(40,vbe,bt1,bt2,bt3,bt4,bt5);
        hBox2.setPrefHeight(350);
        hBox2.setPrefWidth(1700);

        Button bt6 = new Button("caner");
        bt6.setPrefHeight(300);
        bt6.setPrefWidth(250);
        bt6.setStyle("-fx-background-color:aqua");

        Button bt7 = new Button("caner");
        bt7.setPrefHeight(300);
        bt7.setPrefWidth(250);
        bt7.setStyle("-fx-background-color:aqua");

        Button bt8 = new Button("caner");
        bt8.setPrefHeight(300);
        bt8.setPrefWidth(250);
        bt8.setStyle("-fx-background-color:aqua");

        Button bt9 = new Button("caner");
        bt9.setPrefHeight(300);
        bt9.setPrefWidth(250);
        bt9.setStyle("-fx-background-color:aqua");

        Button bt10 = new Button("caner");
        bt10.setPrefHeight(300);
        bt10.setPrefWidth(250);
        bt10.setStyle("-fx-background-color:aqua");


        VBox vbe1 = new VBox();
        vbe1.setPrefHeight(300);
        vbe1.setPrefWidth(110);

        HBox hBox3 = new HBox(40,vbe1,bt6,bt7,bt8,bt9,bt10);
        hBox2.setPrefHeight(350);
        hBox2.setPrefWidth(1700);

        


        VBox vb = new VBox(40,hBox,hBox2,hBox3);
        vb.setPrefHeight(2100);
        hBox2.setAlignment(Pos.CENTER_LEFT);

        ScrollPane scp = new ScrollPane();
        scp.setContent(vb);
        scp.setPrefHeight(2100);
        scp.setPrefWidth(1700);
        scp.setVbarPolicy(ScrollBarPolicy.ALWAYS);
        scp.setHbarPolicy(ScrollBarPolicy.NEVER);

        HBox hBox4 = new HBox(vb,scp);
        hBox4.setPrefWidth(1700);
        hBox4.setPrefHeight(1000);
        
 
        Group gp= new Group(hBox4);

        view.add(gp, 1, 1);

    }
    public GridPane getView(){
        return view;
    }
    
}

